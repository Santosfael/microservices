package com.rafael.core.model;

import java.io.Serializable;

public interface AbstractEntity extends Serializable {
    Long getId();
}
