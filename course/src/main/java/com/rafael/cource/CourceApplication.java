package com.rafael.cource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan({"com.rafael.core.model"})
@EnableJpaRepositories({"com.rafael.core.repository"})
public class CourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourceApplication.class, args);
    }

}
